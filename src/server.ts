import { Application, Router } from "../deps.ts";

const app = new Application();
const router = new Router();

router.get("/", ({ response, params }) => {
  response.type = "application/json";
  response.body = {
    status: true,
    message: "Hello, friend!",
  };

  console.log("foo", params, Deno.env.get("DENO_DIR"));
  debugger;
});

router.get("/hello/:name", ({ response, params }) => {
  response.type = "application/json";
  response.body = {
    status: true,
    message: `Hello, ${params.name}!`,
  };
});

// Logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.headers.get("X-Response-Time");
  console.log(`${ctx.request.method} ${ctx.request.url} - ${rt}`);
});

// Timing
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.response.headers.set("X-Response-Time", `${ms}ms`);
});

app.use(router.routes());
app.use(router.allowedMethods());

await app.listen({ port: 8000 });
app.addEventListener("listen", (params) => {
  console.log("foo", params, Deno.env.toObject());
});
