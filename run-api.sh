#!/usr/bin/env bash

#DENO_DIR=./.cache denon run --allow-net --allow-env "./src/server.ts"

docker build -t app . && docker run -it --init -p 8000:8000 app
